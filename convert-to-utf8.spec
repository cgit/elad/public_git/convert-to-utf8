
Name:         convert-to-utf8
Version:        1.1
Release:        0%{?dist}
Summary:        Useful script that coverts files to UTF-8

Group:          Applications/System
License:        MIT
URL:            http://www.doom.co.il/
BuildRoot:      %{_tmppath}/%{name}
Source:       %{name}.tar.gz
Requires:     GConf2 zenity bash glibc-common
BuildArch:    noarch
Obsoletes: UTF8-Converter
%description
Useful script that coverts files to UTF-8

%prep
%setup -q

%build
%{nil}
%pre
%gconf_schema_prepare %{name}
%install
%{__rm} -rf $RPM_BUILD_ROOT
%{__install} -d $RPM_BUILD_ROOT%{_bindir}
%{__install} -D %{name} $RPM_BUILD_ROOT%{_bindir}/%{name}
%{__install} -d  $RPM_BUILD_ROOT%{_sysconfdir}
%{__install} -d  $RPM_BUILD_ROOT%{_sysconfdir}/gconf
%{__install} -d  $RPM_BUILD_ROOT%{_sysconfdir}/gconf/schemas
%{__install} -D convert-to-utf8.schemas $RPM_BUILD_ROOT%{_sysconfdir}/gconf/schemas/convert-to-utf8.schemas

%post
%gconf_schema_upgrade convert-to-utf8
%preun
%gconf_schema_remove convert-to-utf8

%clean

%{__rm} -fr $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%config %{_sysconfdir}/gconf/schemas/%{name}.schemas

%changelog
* Wed Dec 01 2010 Elad Alfassa <el.il@doom.co.il> 1.1-0
Fix error when trying to convert a file that is already a utf-8 file
* Sun Jul 25 2010 Elad Alfassa <elad@macron.co.il> - 1-0
initial build

